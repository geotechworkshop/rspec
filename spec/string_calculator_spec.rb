require 'spec_helper'

describe StringCalculator do
  before(:all) do
    puts "Before All"
  end

  before(:each) do
    puts "Before Each"
  end

  describe '.add' do
    context 'given an empty string' do
      it 'return zero' do
        expect(StringCalculator.add("")).to eq(0)
      end
    end

    context 'given 4' do
      it 'return 4' do
        expect(StringCalculator.add("4")).to eq(4)
      end
    end

    context 'given 10' do
      it 'return 1' do
        expect(StringCalculator.add("10")).to eq(1)
      end
    end

    context 'given 123' do
      it 'return 6' do
        expect(StringCalculator.add("123")).to eq(6)
      end
    end
  end
end
